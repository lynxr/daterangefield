package ru.lynxr.vaadin.fields.daterangefield;

import com.vaadin.ui.DateField;

import java.util.Date;

/**
 * Created by john on 11.06.16.
 */
public class DateRangeEvent {
    private DateField startDate;
    private DateField endDate;

    public DateRangeEvent(DateField startDate, DateField endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Date getStartDateValue() {
        return startDate.getValue();
    }

    public Date getEndDateValue() {
        return endDate.getValue();
    }
}
