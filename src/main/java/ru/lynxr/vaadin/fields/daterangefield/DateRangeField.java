package ru.lynxr.vaadin.fields.daterangefield;

import com.vaadin.data.Property;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;

/**
 * Created by john on 11.06.16.
 */
public class DateRangeField extends HorizontalLayout {
    private DateField startDate;
    private DateField endDate;

    public DateRangeField(String startCaption, String endCaption) {
        startDate = new DateField(startCaption);
        endDate = new DateField(endCaption);

        addComponent(startDate);
        addComponent(endDate);
    }

    public void setResolution(Resolution resolution) {
        startDate.setResolution(resolution);
        endDate.setResolution(resolution);
    }

    public void addValueChangeListener(final DateRangeValueChangeListener listener) {
        startDate.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                listener.valueChange(new DateRangeEvent(startDate, endDate));
            }
        });
        endDate.addValueChangeListener(new Property.ValueChangeListener() {
            public void valueChange(Property.ValueChangeEvent event) {
                listener.valueChange(new DateRangeEvent(startDate, endDate));
            }
        });
    }
}
