package ru.lynxr.vaadin.fields.daterangefield;

import java.util.EventListener;

/**
 * Created by john on 11.06.16.
 */
public interface DateRangeValueChangeListener extends EventListener {
    void valueChange(DateRangeEvent event);
}
